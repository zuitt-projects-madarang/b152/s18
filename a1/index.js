const trainer = {
	name: "Ash Ketchum",
	age: 10,
	pokemon: ["Pikachu", "Charizard", "Squirtle", "Bulbasaur"],
	friends: {
		kanto: ["Brock", "Misty"],
		hoenn: ["May", "Max"]
	},
	talk: function(){
		console.log(`Pikachu! I choose you!`);
	}
}

function Pokemon(name, lvl, hp, att){
	this.name = name,
	this.level = lvl,
	this.health = hp * 2,
	this.attack = att,
	this.tackle = function(target){
		console.log(this);
		console.log(target);
		console.log(`${this.name} tackled ${target.name}`);
		console.log(`${target.name}'s health is now reduced to ${target.health - this.attack}`);
		target.health -= this.attack;
		if(target.health <= 0){
			target.faint();
		}
	},
	this.faint = function(){
		console.log(`${this.name} fainted`);
	},	
	this.capture = function(){
		trainer.pokemon.push(this.name);
		console.log(`${this.name} was captured.`)
	},
	this.release = function(){
		trainer.pokemon.pop();
		console.log(`${this.name} was released to the wild. Goodbye ${this.name}!`);
	}
}

console.log(trainer);
console.log("Result of dot notation:");
console.log(trainer.name);
console.log("Result of square bracket notation:");
console.log(trainer["pokemon"]);
console.log("Result of talk method:");
trainer.talk();

let pikachu = new Pokemon("Pikachu", 5, 40, 20);
let mewtwo = new Pokemon("Mewtwo", 100, 100, 100);
let arceus = new Pokemon("Arceus", 100, 100, 100);
let zubat = new Pokemon("Zubat", 4, 30, 40);

console.log(pikachu);
console.log(mewtwo);
console.log(zubat);

console.log("PIKACHU VS. ZUBAT");
console.log(pikachu.tackle(zubat));
console.log(pikachu.tackle(zubat));
console.log(pikachu.tackle(zubat));

console.log("A wild Pokemon appeared!");
console.log(mewtwo.capture());
console.log("A wild Pokemon appeared!");
console.log(arceus.capture());
console.log(arceus.release());
